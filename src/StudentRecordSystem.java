package src;

import java.util.HashMap;

public class StudentRecordSystem {
    private HashMap<Integer, Student> students = new HashMap<>();

    public void addStudent(Student student) {
        students.put(student.getID(), student);
        System.out.println("Student added to record system " + student);
    }


    public void removeStudent(int ID) {
        students.remove(ID);
        System.out.println("Student removed from record system by ID " + ID);
    }


    public Student getStudent(int ID) {
        Student student = students.get(ID);
        System.out.println("Get student by ID " + ID + " " + student);
        return student;
    }

    public void displayAllStudents() {
        for (Student student : students.values()) {
            System.out.println("Display Student " +student);
        }
    }
}





