package src;

public class Student {
    private int ID;
    private String name;
    private int GPA;

    public Student(int ID, String name, int GPA) {
        this.ID = ID;
        this.name = name;
        this.GPA = GPA;
    }

    public int getID() {
        return ID;
    }

    public void setID(int ID) {
        this.ID = ID;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getGPA() {
        return GPA;
    }

    public void setGPA(int GPA) {
        this.GPA = GPA;
    }

    @Override
    public String toString() {
        return "Student{" +
                "ID=" + ID +
                ", name='" + name + '\'' +
                ", GPA=" + GPA +
                '}';
    }
}






